*Assignment 1 - Optimal Formation

*We start by assigning all the sets, variables and tables
sets
i set of players /1*25/
j set of positions /GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
k set of formations /442, 352, 4312, 433, 343, 4321/
qp(i) subset of i containg quality players /13, 20, 21, 22/
sp(i) subset of i containing strong players /10, 12, 23/
;


binary variables
x(i,j) takes value 1 if player i plays position j and 0 otherwise
y(k) takes value 1 if formation k i used and 0 otherwise
;

variable
z objective value
;

Table
a(k,j) Matrix showing the different formation and the number of players required for each formation
         GK      CDF     LB      RB      CMF     LW      RW      OMF     CFW     SFW
442      1       2       1       1       2       1       1       0       2       0
352      1       3       0       0       3       1       1       0       1       1
4312     1       2       1       1       3       0       0       1       2       0
433      1       2       1       1       3       0       0       0       1       2
343      1       3       0       0       2       1       1       0       1       2
4321     1       2       1       1       3       0       0       2       1       0
;

Table
b(i,j) Matrix showing what position the different players can play and how fit they are for the different roles
         GK     CDF     LB      RB      CMF     LW      RW      OMF     CFW     SFW
1        10      0       0       0       0       0       0       0       0       0
2        9       0       0       0       0       0       0       0       0       0
3        8.5     0       0       0       0       0       0       0       0       0
4        0       8       6       5       4       2       2       0       0       0
5        0       9       7       3       2       0       2       0       0       0
6        0       8       7       7       3       2       2       0       0       0
7        0       6       8       8       0       6       6       0       0       0
8        0       4       5       9       0       6       6       0       0       0
9        0       5       9       4       0       7       2       0       0       0
10       0       4       2       2       9       2       2       0       0       0
11       0       3       1       1       8       1       1       4       0       0
12       0       3       0       2       10      1       1       0       0       0
13       0       0       0       0       7       0       0       10      6       0
14       0       0       0       0       4       8       6       5       0       0
15       0       0       0       0       4       6       9       6       0       0
16       0       0       0       0       0       7       3       0       0       0
17       0       0       0       0       3       0       9       0       0       0
18       0       0       0       0       0       0       0       6       9       6
19       0       0       0       0       0       0       0       5       8       7
20       0       0       0       0       0       0       0       4       4       10
21       0       0       0       0       0       0       0       3       9       9
22       0       0       0       0       0       0       0       0       8       8
23       0       3       1       1       8       4       3       5       0       0
24       0       3       2       4       7       6       5       6       4       0
25       0       4       2       2       6       7       5       2       2       0
;


*We define the objective function and the constraints
Equations
objective               The objective function maximizes the total fitness player-role
const1                   The first constraint ensures that exactely one formation is chosen
const2(i)                The second constraint ensures that at most one role can be assigned to a player
const3                   The third constraint ensures that at least one qualite player is employed
const4                   The fourth constraint ensures that if all quality players are imployed at least ont strength player is employed
const5(j)                The fifth constraint ensures that all roles in formation must be filled
; 

*The objective value should be equal to the sum over all the players and positions
objective.. z =E= sum((i,j), x(i,j)*b(i,j));

*The first constraint says that the sum over all the formations should be equal to one
*Meaning that y(k) only takes effect for one of the formations
const1..    sum(k, y(k)) =E= 1;

*The second constraint says that for each position only one player can be chosen. This should hold for all players
const2(i).. sum(j, x(i,j)) =L= 1;

*The third constraint says that the sum over the the quality players in all positions should be greater than or equal to one 
const3..    sum((qp(i),j), x(i,j)) =G= 1;

*The fourth constraint says that the sum over all the qulaity players should be less than or equal to the sum of strenght
*players plus the cardinality of the quality players minus 1
const4..    sum((qp(i),j), x(i,j)) =L= sum((sp(i),j), x(i,j)) + 4 - 1;

*The fifth constraint  says the sum over all the players should be equal to the sum of the players in the chosen formation.
*This should be true for all positions
const5(j).. sum(i, x(i,j)) =E= sum(k, a(k,j)*y(k));
     


MODEL FCK /all/;
SOLVE FCK USING mip MAXIMIZING z;
*
DISPLAY
         x.l
         y.l
         z.l;


