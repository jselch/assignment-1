*Fleet Planning

set
v set of ships /v1*v5/
r set of routes /Asia, ChinaPacific/
p set of ports /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
h1(p) set of incompatible ports /Singapore, Osaka/
h2(p) set of incompatible ports /Incheon, Victoria/
;

Parameters
K Number of ports to visit 
F(v) Fixed costs /v1 65, v2 60, v3 92, v4 100, v5 110/
G(v) How much a ship can sail in a year /v1 300, v2 250, v3 350, v4 330, v5 300/
D(p) Minimum amount of times to visit a port /Singapore 15, Incheon 18, Shanghai 32, Sydney 32, Gladstone 45, Dalian 32, Osaka 15, Victoria 18/
;

K = 5;

Table A(p,r) 1 if r passes througth port p and 0 otherwise
            Asia   ChinaPacific
Singapore      1              0
Incheon        1              0
Shanghai       1              1
Sydney         0              1
Gladstone      0              1
Dalian         1              1
Osaka          1              0
Victoria       0              1
;

Table C(v,r) Cost for a ship v to use a route r (in million dollars)
    Asia  ChinaPacific
v1  1.41           1.9
v2   3.0           1.5
v3   0.4           0.8
v4   0.5           0.7
v5   0.7           0.8
;

Table T(v,r) Number of days each ship v need to complete a route r
    Asia  ChinaPacific
v1  14.4          21.2
v2  13.0          20.7
v3  14.4          20.6
v4  13.0          19.2
v5  12.0          20.1
;


Binary variables
u(v) binary variable that takes value 1if ship v is used
y(p) binary variable that takes value 1 if port p is used
;

Variables
z Objective value
;

Integer variable
x(v,r) if ship v sails route r
;


Equations
Obj              "Objective function that minimizes the total yearly costs"
const1         "The first constraint ensures that at least k ports are visited "
const2(v)     "The second constraint says that the number of days it takes to sail a chosen route has to be less than the total amount of sailing days per ship."
const3(p)     "The third constraint says that if you visit port p, you have to do it at least D(p) times"
const4         "The fourth constraint says that you cannot both visit Singapore and Osaka"
const5         "The fifth constraint says that you cannot both visit Incheon and Victoria"
;


Obj.. z =E= sum((v,r), c(v,r)*x(v,r)) + sum(v, u(v)*F(v));

const1.. sum(p, y(p)) =G= K;

const2(v).. sum(r, x(v,r)*T(v,r)) =L= G(v)*u(v);

const3(p).. sum((r,v), A(p,r)*x(v,r)) =G= D(p)*y(p);

const4.. sum(h1(p), y(p)) =L= 1;

const5.. sum(h2(p), y(p)) =L= 1;

 


MODEL fleet /all/;
SOLVE fleet USING mip MINIMIZING z;

DISPLAY
         x.l
         y.l
         z.l
         u.l;


fleet.optcr = 0;








