*MANAGE KEYS
*We start by defining all the sets, parameters and variables
sets 
i set of nodes /1*8/
j set of keys/1*30/
;

*Copying set i to make set ii
alias(i,ii);

Parameter
mk          The space key j occupies
Mi           The memory limit of node i
T             Parameter which ensures that each key is used at most T times
q             Number of keys that two nodes need to share for there to be a direct connection
;

*Assigning values to the parameters
mk=186;
Mi=1000;
T=2;
q=3;

binary variables
x(i,j)          Binary variable that takes the value 1 if node i contains key j and zero otherwise
y(i,ii)         Binary variable that takes the value 1 if there is a connection between node i and node ii and zero otherwise
u(i,ii,j)       Binary variable that takes the value 1 if both node i and node ii contains key j and zero otherwise
;

variables
z              Objective value
;

equations
obj                The objective function which maximizes the number of direct connections
cons1(j)         The first constraint ensures that each key is used at most T times
cons2(i)         The second constraint ensures that each key that occupies space m(k) in node i cannot exceed the limit Mi
cons3(i,ii,j)    The third constraint is used to create a variable that takes the value 1 if both node i and node ii containts key j 
cons4(i,ii,j)    The fourth constraint is used to create a variable that takes the value 1 if both node i and node ii containts key j 
cons5(i,ii)      The fifth constraint says that there has to be at least three keys before there is a direct connection
;

obj.. z =E= sum((i,ii)$(ord(i) lt ord(ii)), y(i,ii));

cons1(j).. sum(i, x(i,j)) =L= T;

cons2(i).. sum(j, x(i,j)*mk) =L= Mi;

cons3(i,ii,j).. x(i,j)+x(ii,j)$(ord(i) lt ord(ii)) =L= 1 + u(i,ii,j);

cons4(i,ii,j).. x(i,j)+x(ii,j)$(ord(i) lt ord(ii)) =G= 2*u(i,ii,j);

cons5(i,ii).. sum(j, u(i,ii,j)) =G= q*y(i,ii);

*In constraint 3 and 4 and in the objective function we only sum over the nodes where node i < node ii.
*This is done to prevent a node sending a connection to it self og a node ii sending a connection back to node i.



MODEL production /all/;
SOLVE production USING mip MAXIMIZING z;
*
DISPLAY
         x.l
         y.l
         z.l;